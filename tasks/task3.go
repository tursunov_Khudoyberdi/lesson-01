package main

import (
	"fmt"
)

const P float32 = 3.14

func main() {
	var r float32 = 10.04

	// fmt.Scanf("%f", &r)
	fmt.Println("R =", r)

	fmt.Printf("Area: %0.2f\n", area(r))
}

func area(r float32) (area float32) {
	result := (2 * r) * (2 * r)
	area = result - P*r*r
	return area
}
